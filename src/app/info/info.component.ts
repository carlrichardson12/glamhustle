import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {

  constructor() { }

  content: InfoContent = {
    title: 'Policy',
    subTitle: 'No Call/ No Show Policy',
    policyParagraph1: 'If you will not be able to make it' +
      'to your appointment, please be considerate and text me' +
      ' to let me know you will not be showing up. Otherwise All' +
      'no call/no show clients will be BLOCKED from my booking site!',
    subTitle2: 'Late Policy:',
    policyParagraph2: 'If a client arrives 10 minutes late they will' +
      'be charged an additional $15. If a client fails to show up for their' +
      'appointment after 15 minutes, their appointment will automatically be cancelled',
      button: 'Book an appointment'
  };

  pricing = {
    wigs: {
      short: {
        length1: [12, 13, 14, 15, 16, 17, 18],
        value: '190.00'
      },
      long: {
        length2: [20, 21, 22, 23, 24, 25, 26],
        value: '260.00'
      }
    },
    texture: {
      value: '10.00',
      userWanted: false,
      name: 'Texture'
    },
    frontal: {
      value: '20.00',
      userWanted: false,
      name: 'Frontal'
    },
    color: {
      value: '45.00',
      userWanted: false,
      name: 'Color'
    },
    title: 'Get an Custom Estimate',
    total: 0
  };

  userInput = {
    wigLength: 1
  };

  generalPricing = {
    title: 'General Pricing',
      closeSew: {
        value: '80.00',
        userWanted: false,
        name: 'Closure Sew-in',
        desc: ''
      },
      frontSew: {
        value: '95.00',
        userWanted: false,
        name: 'Frontal Sew-in',
        desc: ''
      },
      wigConstruct: {
        value: '80.00',
        userWanted: false,
        name: 'Wig Construction',
        desc: 'bleaching knot and plucking included'
      },
      wigInstall: {
        value: '165.00',
        userWanted: false,
        name: 'Wig install',
        desc: ''
      },
      closureTup: {
        value: '25.00',
        userWanted: false,
        name: 'Closure Touch Up ',
        desc: ''
      },
      frontalTup: {
        value: '30.00',
        userWanted: false,
        name: 'Frontal Touch Up',
        desc: ''
      },
      total2: 0
  };

  hairColoring = {
    black: {
      value: '25.00',
      userWanted: false,
      name: 'Black Coloring',
      color: 'black'
    },
    custom: {
      value: '25.00',
      userWanted: false,
      name: 'Frontal Touch Up',
      color: ''
    }
  };

  autoTicks = false;
  disabled = false;
  invert = false;
  max = 26;
  min = 12;
  showTicks = false;
  step = 1;
  thumbLabel = false;
  vertical = false;
  tickInterval = 1;

  panelOpenState = false;
  ngOnInit() {
  }

  ngDoCheck() {
    this.getTotal();
  }

  getTotal() {
    const addtions = this.pricingItems();
    const wigs = this.wigPricing();
    const general = 
    this.pricing.total = wigs + addtions;
    this.generalPricing.total2 = this.genPricing();
  }


  getSliderTickInterval(): number | 'auto' {
    if (this.showTicks) {
      return this.autoTicks ? 'auto' : this.tickInterval;
    }
    return 0;
  }

  pricingItems() {
    const valueArray = [];
    let sum: number;

    if (this.pricing.color.userWanted) {
      valueArray.push(parseFloat(this.pricing.color.value));
    }

    if (this.pricing.texture.userWanted) {
      valueArray.push(parseFloat(this.pricing.texture.value));
    }

    if (this.pricing.frontal.userWanted) {
      valueArray.push(parseFloat(this.pricing.frontal.value));
    }
    sum = valueArray.reduce((a, b) => a + b, 0);
    return sum;
  }

  wigPricing() {
    if (this.userInput.wigLength < 20 && this.userInput.wigLength >= 12) {
      return parseFloat(this.pricing.wigs.short.value);
    }

    if (this.userInput.wigLength >= 20) {
      return parseFloat(this.pricing.wigs.long.value);
    }
  }

  genPricing() {
    const valueArray = [];
    let sum: number;

    if (this.hairColoring.black.userWanted) {
      valueArray.push(parseFloat(this.hairColoring.black.value));
    }

    if (this.hairColoring.custom.userWanted) {
      valueArray.push(parseFloat(this.hairColoring.custom.value));
    }

    if (this.generalPricing.closeSew.userWanted) {
      valueArray.push(parseFloat(this.generalPricing.closeSew.value));
    }


    if (this.generalPricing.frontSew.userWanted) {
      valueArray.push(parseFloat(this.generalPricing.frontSew.value));
    }

    if (this.generalPricing.closureTup.userWanted) {
      valueArray.push(parseFloat(this.generalPricing.closureTup.value));
    }

    if (this.generalPricing.frontalTup.userWanted) {
      valueArray.push(parseFloat(this.generalPricing.frontSew.value));
    }

    if (this.generalPricing.wigConstruct.userWanted) {
      valueArray.push(parseFloat(this.generalPricing.wigConstruct.value));
    }

    if (this.generalPricing.wigInstall.userWanted) {
      valueArray.push(parseFloat(this.generalPricing.wigInstall.value));
    }

    sum = valueArray.reduce((a, b) => a + b, 0);
    return sum;
  }



}

export interface InfoContent {
  title: string;
  subTitle: string;
  policyParagraph1: string;
  subTitle2: string;
  policyParagraph2: string;
  button: string;
}

export interface PricingContent {
  wigs: [any];

}