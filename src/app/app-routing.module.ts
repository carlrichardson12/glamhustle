import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { InfoComponent } from './info/info.component';
import { ApointmentBookerComponent } from './apointment-booker/apointment-booker.component';

const routes: Routes = [
  { path: 'Home', component: HomeComponent},
  { path: 'Info', component: InfoComponent},
  { path: 'Booking', component: ApointmentBookerComponent},
  { path: '**', component: HomeComponent},
  { path: '', component: HomeComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
