import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApointmentBookerComponent } from './apointment-booker.component';

describe('ApointmentBookerComponent', () => {
  let component: ApointmentBookerComponent;
  let fixture: ComponentFixture<ApointmentBookerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApointmentBookerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApointmentBookerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
