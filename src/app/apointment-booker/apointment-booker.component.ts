import { Component, OnInit, ViewChild, TemplateRef, ChangeDetectionStrategy } from '@angular/core';
import { AppointmentService, Appointments } from './services/aptService';
import { CalendarView, CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours, addMinutes } from 'date-fns';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';


@Component({
  selector: 'app-apointment-booker',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './apointment-booker.component.html',
  styleUrls: ['./apointment-booker.component.css']
})
export class ApointmentBookerComponent implements OnInit {
  @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any>;
  booking = {
    title: 'Times Avalible',
    today: new Date(),
    time: [],
    day: new Date(),
    searchTime: '',
    weeks: [],
    testStuff: ''
  };

  isTakenTime = false;

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  matcher = new MyErrorStateMatcher();

  content: any = {
    calendarTitleDay: 'Available Times Today',
    calendarTitleWeek: 'Available Times This Week'
  };

  dateControl = new FormControl(new Date());
  allAptsArry;
  testStuff3: Appointments;

  time: any;
  day: any;
  work = localStorage.getItem('work');
  temp: any = {
    time: '',
    day: '',
    end: addHours(new Date(), 1)
  };

  newApts: NewAppointment = {
    fName: '',
    lName: '',
    email: '',
    date: '',
    end: ''
  };
  view: CalendarView = CalendarView.Day;
  CalendarView = CalendarView;
  viewDate: Date = new Date();
  modalData: {
    action: string;
    event: CalendarEvent;
  };


  actions: CalendarEventAction[] = [
    {
      label: '<i class="fas fa-fw fa-pencil-alt"></i>',
      a11yLabel: 'Edit',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      },
    },
    {
      label: '<i class="fas fa-fw fa-trash-alt"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter((iEvent) => iEvent !== event);
        this.handleEvent('Deleted', event);
      },
    },
  ];

  refresh: Subject<any> = new Subject();


  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = true;

  constructor(private apt: AppointmentService, private modal: NgbModal, private dialog: MatDialog) { }



  ngOnInit() {
    this.allApts();
    // this.submitAptApi(this.submitApt);
  }

  noDoCheck() {
    console.log(this.newApts);
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd,
  }: CalendarEventTimesChangedEvent): void {
    this.events = this.events.map((iEvent) => {
      if (iEvent === event) {
        return {
          ...event,
          start: newStart,
          end: newEnd,
        };
      }
      return iEvent;
    });
    this.handleEvent('Dropped or resized', event);
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addEvent(): void {
    this.events = [
      ...this.events,
      {
        title: 'New event',
        start: startOfDay(new Date()),
        end: endOfDay(new Date()),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true,
        },
      },
    ];
  }

  deleteEvent(eventToDelete: CalendarEvent) {
    this.events = this.events.filter((event) => event !== eventToDelete);
  }

  setView(view: CalendarView) {
    this.view = view;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  allApts() {
    this.apt.getList().subscribe(
      data => this.handleSuccess(data)
    );
    console.log(this.allAptsArry + ' 2');
  }

  private handleSuccess(data) {
    this.allAptsArry = data;
    console.log(this.allAptsArry);
    this.putEvents(this.allAptsArry);
  }

  private submitAptApi(apt: NewAppointment) {
    return this.apt.saveApt(apt).subscribe(data => {
      this.testStuff3 = data;
      console.log(this.testStuff3);
    });
  }

  // finsigh making the object 
  private putEvents(apts: Appointments[]) {
    apts.forEach(apt => {
      const tempApt: CalendarEvent = {
        start: new Date(Date.parse(apt.date)),
        end: apt.end ? new Date(Date.parse(apt.end)) : addHours(Date.parse(apt.date), 1),
        title: apt.work,
      };
      this.events.push(tempApt);
    });
  }

  onSubmit() {
    const hrs = parseInt(localStorage.getItem('hr'));
    const min = parseInt(localStorage.getItem('min'));
    this.newApts.end = addHours(Date.parse(this.day), hrs);
    this.newApts.end = addMinutes(this.newApts.end, min);
    if (localStorage.getItem('work')) {
      this.newApts.work = localStorage.getItem('work');
    }

    if (this.isTaken()) {
      this.newApts.date = this.day;
      console.log(this.newApts.end);
      console.log(this.newApts.date);
      this.submitAptApi(this.newApts);
      localStorage.clear();
      window.location.reload();
    }
  }

  isTaken() {
    const bookedTimes = this.events;
    let isntTaken = true;
    bookedTimes.forEach(booked => {
      if (booked.start === this.newApts.date && booked.end === this.newApts.end) {
        isntTaken = false;
        const dialogRef = this.dialog.open(DialogContentExampleDialog);

        dialogRef.afterClosed().subscribe(result => {
          console.log(`Dialog result: ${result}`);
        });
      }
    });
    return isntTaken;
  }


}

export interface NewAppointment {
  fName: string;
  lName: string;
  email: string;
  date?: any;
  work?: any;
  end: any;
}

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3',
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF',
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA',
  },
};

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-dialog-modal',
  templateUrl: 'dialog-content.html',
})
export class DialogContentExampleDialog {}

/*  other codde... may not use
 events: CalendarEvent[] = [
   {
     start: subDays(startOfDay(new Date()), 1),
     end: addDays(new Date(), 1),
     title: 'A 3 day event',
     color: colors.red,
     actions: this.actions,
     allDay: true,
     resizable: {
       beforeStart: true,
       afterEnd: true,
     },
     draggable: true,
   },
   {
     start: startOfDay(new Date()),
     title: 'An event with no end date',
     color: colors.yellow,
     actions: this.actions,
   },
   {
     start: subDays(endOfMonth(new Date()), 3),
     end: addDays(endOfMonth(new Date()), 3),
     title: 'A long event that spans 2 months',
     color: colors.blue,
     allDay: true,
   },
   {
     start: addHours(startOfDay(new Date()), 2),
     end: addHours(new Date(), 2),
     title: 'A draggable and resizable event',
     color: colors.yellow,
     actions: this.actions,
     resizable: {
       beforeStart: true,
       afterEnd: true,
     },
     draggable: true,
   },
 ];
 */
