import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { NewAppointment } from '../apointment-booker.component';

@Injectable({
    providedIn: 'root'
})

export class AppointmentService {
    // private api: any = {
    //     newApt: 'http://Api2-env.eba-rbg5rghh.us-east-2.elasticbeanstalk.com/appointment/newApt',
    //     getApt: 'http://Api2-env.eba-rbg5rghh.us-east-2.elasticbeanstalk.com/appointment/all-apts'
    // };

    private api: any = {
        newApt: 'http://localhost:5000/appointment/newApt',
        getApt: 'http://localhost:5000/appointment/all-apts'
    };

    constructor (private httpCli: HttpClient) {}

     getList(): Observable<Appointments[]> {
        return this.httpCli.get<Appointments[]>(this.api.getApt);
    }

     saveApt(req: NewAppointment) {
        const save = this.httpCli.post<any>(this.api.newApt, req);
        return save;
    }


}

export class Appointments {
  fName: string;
  lName: string;
  email: string;
  date?: any;
  work?: any;
  end: any;
}



