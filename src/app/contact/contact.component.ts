import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  contactMe = {
    title: 'Contact Me',
    email: 'glamHustleco@gmail.com',
    phone: '(386) 315-3240',
    isOpen: true,
    open: 'Open Until 5p.m.',
    daysArry: [
      {
        day: 'Sorry We are Closed'
      },
      {
        day: 'Monday 9:00 AM - 5:00 PM'
      },
      {
        day: 'Tuesday 9:00 AM - 5:00 PM'
      },
      {
        day: 'Wednesday 9:00 AM - 5:00 PM'
      },
       {
        day: 'Thursday 9:00 AM - 5:00 PM'
      },
      {
        day: 'Friday 9:00 AM - 5:00 PM'
      },
       {
        day: 'Saturday 9:00 AM - 5:00 PM'
      },
       {
        day: 'Sunday Closed'
      },
    ]
  };

  constructor() { }

  ngOnInit() {
  }

  ngDoCheck() {
   this.contactMe.isOpen = this.isOpen();
   console.log(new Date().getHours());
  }

  isOpen(): boolean {
    const input = new Date();
    const day = new Date(input) ;
    let isDay = false;
    if ( day.getHours() > 17 && day.getHours() > 9 ) {
      isDay = true;
    }

    if (day.getDay() === 0) {
    isDay = false;
    }
    return isDay;
  }

}
