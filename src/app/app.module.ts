import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ToolBarComponent } from './tool-bar/tool-bar.component';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import {  NgbModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { InfoComponent } from './info/info.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule, MatExpansionModule, MatFormFieldModule, MatInputModule, MatSliderModule, MatCheckboxModule, MatDatepickerModule} from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApointmentBookerComponent, DialogContentExampleDialog } from './apointment-booker/apointment-booker.component';
import { PricingListComponent } from './pricing-list/pricing-list.component';
import { ContactComponent } from './contact/contact.component';
import { AppointmentService } from './apointment-booker/services/aptService';
import { HttpClientModule } from '@angular/common/http';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { DemoMaterialModule } from './app-matieral.module';

@NgModule({
  declarations: [
    AppComponent,
    ToolBarComponent,
    HomeComponent,
    InfoComponent,
    ApointmentBookerComponent,
    PricingListComponent,
    ContactComponent,
    DialogContentExampleDialog
  ],
  imports: [
    BrowserModule,
    NgbModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSliderModule,
    MatCheckboxModule,
    HttpClientModule,
    NgbModalModule,
    MatDatepickerModule,
    DemoMaterialModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory })
  ],
  providers: [AppointmentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
