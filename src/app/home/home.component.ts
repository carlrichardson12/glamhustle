import { Component, OnInit } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [NgbCarouselConfig]  // add NgbCarouselConfig to the component providers
})
export class HomeComponent implements OnInit {
  showNavigationArrows = false;
  showNavigationIndicators = true;
  images = [
  'https://glamhustle.s3.us-east-2.amazonaws.com/headshot1.png',
  'https://glamhustle.s3.us-east-2.amazonaws.com/wighead1.png',
  'https://glamhustle.s3.us-east-2.amazonaws.com/headshot2.png',
  'https://glamhustle.s3.us-east-2.amazonaws.com/wighead2.png',
  'https://glamhustle.s3.us-east-2.amazonaws.com/Headshot5.jpg',
  'https://glamhustle.s3.us-east-2.amazonaws.com/girlboss1.png',
  'https://glamhustle.s3.us-east-2.amazonaws.com/wighand1.jpg'

  ] ;
  constructor(private config: NgbCarouselConfig) {
      // customize default values of carousels used by this component tree
      config.showNavigationArrows = true;
      config.showNavigationIndicators = true;
   }

  ngOnInit() {
  }

}
