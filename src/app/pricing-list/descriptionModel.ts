
export class DescriptionModel {
    constructor() { }
    fullDescription = {
      title: 'List of Services, Description, & Time Expectations',
      headTitle: 'List of Services',
      desc: [
        {
          name:'Tradtional Sew-in',
          description: 'Install of 3 with Leave Out (Your Hair)',
          value: '60.00',
          min: '30',
          hr: '1',
          userWanted: false
        },
        {
          name:'Clousure Sew-In',
          description: 'This Service includes install of 3 bundles and a closure. Closure Must be dropped off 24 hours prior',
          value: '80.00',
          min: '10',
          hr: '2',
          userWanted: false
        },
        {
          name:'Frontal Sew-In',
          description: 'This Service includes install of 3 bundles and a closure. Closure Must be dropped off 24 hours prior',
          value: '95.00',
          min: '35',
          hr: '2',
          userWanted: false
        },
        {
          name:'Wig Construction + Install',
          description: 'Service Includes construction, customization, & installation',
          value: '100.00',
          min: '30',
          hr: '1',
          userWanted: false
        },
        {
          name: 'Wig Install',
          description: ' Wig Must be dropped off a day prior to your appointment',
          value: '60.00',
          min: '45',
          hr: '1',
          userWanted: false
        },
        {
          name: 'Milk & Honey Bath Manicure & Pedicure',
          description: 'A Warm milk bath with honey and sugar scrub & light massage with a regular polish',
          value: '45.00',
          min: '45',
          hr: '0',
          userWanted: false
        },
        {
          name:'Crochet',
          description: 'Corn-Roll Method',
          value: '35.00',
          min: '30',
          hr: '1',
          userWanted: false
        },
        {
          name: 'Bring A Friend Tradtional Sew-in',
          description: 'Must come same day',
          value: '85.00',
          min: '0',
          hr: '4',
          userWanted: false
        },
        {
          name:'Bring A Friend Clousure Sew-In',
          description: 'Must come same day',
          value: '140.00',
          min: '0',
          hr: '4',
          userWanted: false
        },
        {
          name:'Bring A Friend Wig Construction + Install',
          description: 'Service Includes construction, customization, & installation',
          value: '140.00',
          min: '0',
          hr: '4',
          userWanted: false
        },
        {
          name:'Volunteer Hair',
          description: ' Anything I ask to try I will do it',
          value: '45.00',
          min: '30',
          hr: '0',
          userWanted: false
        },
        {
          name:'Frontal Maintenance',
          description: 'Services includes removal, cleaning and reinstallment of frontal lace only.',
          value: '30.00',
          min: '30',
          hr: '1',
          userWanted: false
        },
        {
          name:'Closure Maintenance',
          description: 'Services includes removal, cleaning and reinstallment of closure lace only.',
          value: '20.00',
          min: '0',
          hr: '1',
          userWanted: false
        },
        {
          name: 'Coloring',
          description: 'Services includes customizing of color of 3 bundles'
          + '+ frontal or closure color must be verified. + 10$ for bleaching black hair',
          value: '30.00',
          min: '30',
          hr: '1',
          userWanted: false
        },
        {
          name: 'Hair Reviving',
          description: 'Services includes restoring used hair Only. Up to 3 bundles'
          + '+ frontal or a wig',
          value: '30.00',
          min: '30',
          hr: '1',
          userWanted: false
        },
        {
          name: 'Half Up Half Down',
          description: '',
          value: '50.00',
          min: '40',
          hr: '1',
          userWanted: false
        }
      ]
    };
  }