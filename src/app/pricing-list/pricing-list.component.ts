import { Component, OnInit } from '@angular/core';
import { DescriptionModel } from './descriptionModel';
import { ApointmentBookerComponent } from '../apointment-booker/apointment-booker.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pricing-list',
  templateUrl: './pricing-list.component.html',
  styleUrls: ['./pricing-list.component.css']
})
export class PricingListComponent implements OnInit {
  fullDescription = new DescriptionModel().fullDescription;
  total = 0;
  content: any = {
    button: 'Book and Appointment'
  };
  work = '';
  constructor(private router: Router) { }

  ngOnInit() {
  }

  ngDoCheck() {
    this.total = this.getPricing();
    this.work = this.getWork();
  }

  getWork() {
    const workStuff = this.fullDescription.desc;
    let work = '';
    workStuff.forEach(works => {
      if (works.userWanted) {
        work = works.name;
      }
    });
    return work;
  }

  getPricing() {
    const descArray = this.fullDescription.desc;
    const totalAray = [];
    let total = 0;
    descArray.forEach(desc => {
      if (desc.userWanted) {
        totalAray.push(parseFloat(desc.value));
      }
    });
    total = totalAray.reduce((a, b) => a + b, 0);
    return total;
  }

  getHr(): string {
    const hrArry = this.fullDescription.desc;
    let hr = '';
    hrArry.forEach(hrs => {
      if (hrs.userWanted) {
        hr = hrs.hr;
      }
    });
    return hr;
  }

  getMin(): string {
    const minArry = this.fullDescription.desc;
    let minute = '';
    minArry.forEach(mins => {
      if (mins.userWanted) {
        minute = mins.min;
      }
    });
    return minute;
  }

  onSubmit() {
    localStorage.removeItem('work');
    localStorage.setItem('work', this.work);
    localStorage.setItem('hr', this.getHr());
    localStorage.setItem('min', this.getMin());
    this.router.navigate(['Booking']);
  }

}
