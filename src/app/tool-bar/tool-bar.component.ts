import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tool-bar',
  templateUrl: './tool-bar.component.html',
  styleUrls: ['./tool-bar.component.css']
})
export class ToolBarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

      // Responsive NavBar Ish
      dropDownClick() {
        const x = document.getElementById('nava');
        if (x.className === 'topnav') {
          x.className += 'responsive';
          const icon = document.getElementById('icon-hamburger');
          icon.style.display = 'hidden';
        } else {
          x.className = 'topnav';
        }
      }
}
